```mermaid
graph TD
    A[_] -->|Yes| B
    A[Do you need to grab the users attention?<br> Is the user making a selection from a small range of options? <br> Is it essential to collect a small amount of information before letting users proceed?<br> Do you need to provide some additional content or features without losing the context of the parent page?<br> Is it urgent the user acknowledge the content?] -->|No| C(New page or inline content) 
    B[_] -->|Yes| D(New page or inline content)
    B[Will users want to bookmark the content or share it?<br> Is your design intended to be used primarily on a mobile device?<br> Is the user leaving the current workflow and not expected to return?<br> Are there different interactions and are they somewhat complex?<br> Is the interaction complex enough to require multiple steps? <br> Are you expecting additional actions, tasks, or content in the future? ] --> | No | E
    E[_] -->|Yes| F
    E[Are users frequently moving back and forth between features of the design?<br> Are users interacting minimally with the features of the design?<br> Is the content temporary?<br> Is the design a transition state?<br> Is the focus of the users to be returned to the originating page after?<br> Do you have a clear and compelling case for why this content should be presented within a type of overlay instead of a page?<br> If the user is viewing content, is the content short and simple?<br> Is the content appearing at the deepest level of the information architecture in the current user flow?] -->|No | G(Non-modal Component)
    F[_] -->|Yes| H(Dialog<br> - Alert<br> - Toast) 
    F[Is this an error or success message?] -->|No| I
    I[_] -->|Yes| J(Modal)
    I[Do you need to collect information from the user?<br> Are you confirming a user action?<br> Will the action have serious consequences and be difficult to reverse?<br> Is the user completing a form or making a decision?<br> Is your content an important warning that is preventing or correcting a critical error?<br> If your content is related to an error is this error irreversible? ] -->|No| K(Drawer)
```
