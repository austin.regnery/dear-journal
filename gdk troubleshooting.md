Because I can never seem to remember how to update this thing properly.

- `gdk docotor` - diagnosis what's wrong
- `gdk reconfigure` – Initialize the gitlabhq_development database
- `bundle install` - installs missing gems
- `gdk install` - installs everything that might be missing
- `bundle exec rails db:migrate` - migrate database?
- `gdk update` – to update gdk once everything is in order


for Compliance Dashboard while **database is running**
- Run `bundle exec rake dev:setup RAILS_ENV=development && bundle exec rake db:migrate RAILS_ENV=development` (this will wipe your local DB)
- Run `bundle exec rake db:seed_fu FILTER=compliance_dashboard_merge_requests` to only run this one seeder



