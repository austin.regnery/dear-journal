### WEEK 33 (2020), MILESTONE [13.3](https://gitlab.com/gitlab-org/gitlab/-/analytics/issues_analytics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acompliance&milestone_title=13.3&assignee_username=aregnery) – W4
- [x] Brainstorm the project view for compliance dashboard
- [x] [Customize my terminal experience](https://medium.com/@caulfieldOwen/youre-missing-out-on-a-better-mac-terminal-experience-d73647abf6d7)
- [x] Customize my VS Code experience
- [x] Fill out [Mural board for career growth](https://app.mural.co/t/gitlab2474/m/gitlab2474/1594045008555/7fed9cd40e226dda501db2c433bf364821998325) - complete by the 17th
- [x] `Finish` downscoping MVC proposal for compliance project labels
  - [ ] Seek & review feedback
- [x] Examine more of the feature set within competitor apps
  - [x] Watch compliance opportunity video
  - [x] Read more about Aptible
- [x] Triage `To-Do's`
- [x] Work with Jarek
  - [ ] `Finish` [!38939](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/38939)
  - [ ] `Finish` [!38966](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/38966)




~"Unplanned Tasks"
- [x] Make a video showing comparison of features
- [x] Feedback in [#ux_working](https://gitlab.slack.com/archives/CLW71KM96/p1597184252244300)
- [x] Submit MR for a [Design Suggestion Template](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/39448)

<!-- ~"To-Do::Next Week" -->

~Backlog

- [ ] Read [Market Guide for Compliance Automation Tools in DevOps](https://drive.google.com/file/d/1Gt-zKKZVdaE62Hu_35HKy9uLXE2QKhvn/view)
- [ ] Read through [&1385](https://gitlab.com/groups/gitlab-org/-/epics/1385) to better understand GitLab's UX History
- [ ] Open an issue to update what information is displayed in the breadcrumb
- [ ] Identify 1st time experience experiments for [#1192](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1192) | &3839 (Approval Page)
- [ ] What is a feature flag?
- [ ] Create ~"UX debt" issues based on this [comment](https://gitlab.com/gitlab-org/gitlab/-/issues/219567#note_390783966)
- [ ] [Brainstorm visionary ideas for Compliance](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/59216#note_393578986)









