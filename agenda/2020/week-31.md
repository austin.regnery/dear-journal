### Monday
---
- [x] Get feedback on design suggestions
- [x] Look into pipeline needs
- [x] Draft presentation ideas for UX Showcase on Aug 5, 2020
  - Playing catchup with the backlog as a new designer to GitLab
  - Making a destructive modal scary
- [x] Open JTBD Recruiting issue for Compliance
- [x] [Open new issue](https://gitlab.com/gitlab-org/gitlab/-/issues/232537) `UX Debt` for the scary modal refinement
- [x] Collab with Mike

### Tuesday
---
🏖 Folly Beach!


### Wednesday
---
- [x] 🆕 To-Do's & MR
- [x] Draft slides for UX Showcase


### Thursday
---
- [x] [Interim solution for exporting project audit](https://gitlab.com/gitlab-org/gitlab/-/issues/213364#proposal)
- [x] [Watch pipelines walkthrough](https://youtu.be/jnA08YexWgw?t=389)
- [x] Review [Two-person approvals for sensitive changes](https://gitlab.com/groups/gitlab-org/-/epics/3839) epic
  - [x] Consolidate related issues into one epic on Figma
- [x] Schedule more ☕️ chats from [Compliance](https://about.gitlab.com/handbook/product/product-categories/#compliance-group)

### Friday
---
🤵🏼👰🏼 Friends' wedding festivities 


### Backlog
---
- [ ] Continue with `P1` issues for `Next Up`
- [ ] Read through [&1385](https://gitlab.com/groups/gitlab-org/-/epics/1385) to better understand GitLab's UX History
- [ ] Read more about the [360 feedback process](https://about.gitlab.com/handbook/people-group/360-feedback/)
- [ ] Create career development plan
  - [ ] Read [handbook page on career development]
  - [ ] Fill out [Mural board for career growth](https://app.mural.co/t/gitlab2474/m/gitlab2474/1594045008555/7fed9cd40e226dda501db2c433bf364821998325)
  - [ ] Discuss with Mike
  - [ ] Check off in Bamboo History
- [ ] Read [Market Guide for Compliance Automation Tools in DevOps](https://drive.google.com/file/d/1Gt-zKKZVdaE62Hu_35HKy9uLXE2QKhvn/view)
- [ ] Look for ways to show context to the team when it comes to the user journey
- [ ] Create alias for GDK
- [ ] Open an issue to propose a change to the date picker component
- [ ] Open an issue to update what information is displayed in the breadcrumb
- [ ] Migrate another button component
- [ ] Identify 1st time experience experiments for [#1192](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1192)





