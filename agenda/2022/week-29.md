## WEEK 29 (2022), MILESTONE 15.3

### ⭐️ Goal: Have nav concepts ready to test

**Monday**

- [x] Vacation 🌴

**Tuesday**

- [x] Coffee chat
- [x] Catch up on mentions and address important questions
- [x] UX Weekly
- [x] Sync with Jeremy
- [x] Review UX Research plan
- [x] Refine mid-fi mocks


**Wednesday**

- [x] Record UX Showcase
- [x] Populate agendas
- [x] Watch Unfiltered
- [x] Sync meetings x4

**Thursday**

- [x] UX Reviews
- [x] Sync meetings
- [x] To-Do List items

**Friday**

- [x] Reproduce race condition
- [x] Read blog on Ruby on Rails
- [x] Investigate Push Rules
- [x] Test plan review for navigation concepts
- [x] Refine designs as needed
