## WEEK 24 (2022), MILESTONE 15.1

### ⭐️ Goal: Finish first round of testing 🍔 menu

**Monday**

- [x] Populate agendas
- [x] Sync meetings
- [x] Get Pajamas Migration Day merge requests intro review
- [x] Review UserTesting sessions
- [x] Start MacBook setup

**Tuesday**

- [x] Fix merge conflicts
- [x] Continue MacBook setup
- [x] To-Do List triage
- [x] Few sync meetings
- [x] UserTesting sessions review

**Wednesday**

- [x] GDK setup
- [x] Agenda population
- [x] UserTesting sessions review for Test B
- [x] Launch Test C
- [x] Sync meetings
- [x] Follow up on 15.1 merge requests
- [x] Coffee chat

**Thursday**

- [x] Finish Test C
- [x] Write up first impressions
- [x] Follow up on mentions
- [x] Sync with Nick

**Friday**

- [x] UX Research Synth
- [ ] Refine issues for 15.2
- [ ] Work on menu menu design ideas
- [x] Coffee chat

#### ♻️ Thoughts & Feelings

UX Research work took up more time than I anticipated

#### 🗄 Backlog

<details><summary>Ideas for another day</summary>

- [ ] Pick 3 items for seeking community contributions
- [ ] Settings needs some backlog love
- [ ] Check in on alerts improvements
- [ ] Refine Unboxing the Advanced Section epic

</details>
