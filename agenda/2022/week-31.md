## WEEK 31 (2022), MILESTONE 15.3

### ⭐️ Goal: Start testing nav concepts

**Monday**

- [x] Project deletion bug testing
- [x] Define success for tasks in concept testing
- [x] UX review the push rules change
- [x] User ID api requests questions
- [x] Feedback on the operations change proposal
- [x] Look into paternity leave documentation again

**Tuesday**

- [x] UX review metrics chart on mobile
- [x] Project deletion issue split
- [x] To-Do triage
- [x] UX Weekly
- [x] UX review avatar changes
- [x] Coffee chat
- [x] MECC training
- [x] Postman inspired dashboard page

**Wednesday**

- [x] Open 2 Hackathon issues
- [x] Community contribution UX review
- [x] 4 sync meetings
- [x] Reply to mentions
- [x] Watch 2nd pilot test

**Thursday**

- [x] Community contribution UX review
- [x] Sync with Marcel
- [x] Work on refining the project deletion bug scope
- [x] Respond to a few mentions
- [x] Watch the next navigation concept testing video
- [x] Update coverage issue
- [x] Sync with Nick

**Friday**

- [x] Look for 11px mono examples
- [x] Avatar UX review
- [x] UX review on Kubernetes integration
- [x] Mentions to reply to
- [x] Watch first user concept test
 
