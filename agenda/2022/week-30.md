## WEEK 30 (2022), MILESTONE 15.3

### ⭐️ Goal: Start testing nav concepts

**Monday**

- [x] Wipe old laptop
- [x] Fix merge conflict in handbook change
- [x] Review test plan comments and adjustments
- [x] Task 3 ideas
- [x] Make Figma file for Olena
- [x] Review Nick's dropdown idea
- [x] Review interview plan
- [x] Crush To-Do List

**Tuesday**

- [x] Empty state info
- [x] Groups push rules MVC
- [x] Prep for interview
- [x] UX Weekly 
- [x] Coffee chat
- [x] Lead interview
- [x] Fill out Greenhouse Scorecard
- [x] Test the VoiceOver race condition

**Wednesday**

- [x] Few mentions in To-Do's
- [x] Circle back to tasks
- [x] Review UX for avatar background
- [x] Weekly navigation sync
- [x] Build another patch for avatars

**Thursday**

- [x] Project deletion bug investigation
- [x] Menu menu ideas
- [x] Nav sync with Nick

**Friday**

- [x] UX reviews
- [x] To-do mentions
- [x] Menu menu dashboard replacement
