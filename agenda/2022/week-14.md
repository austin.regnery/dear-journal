## WEEK 14 (2022), MILESTONE 14.10

### ⭐️ Goal: Get all my assigned merge requests to ship

**Monday**
- [x] To-do responses
- [x] Test the Shopify deployment template
- [x] Test simulating SaaS in GitPod
- [x] Open a merge request to update GitPod docs in gdk
- [x] Note taking for user research session
- [x] Fix Learn GitLab MR
- [x] Check on Compliance issues and merge request
- [x] Look for navigation problems on Twitter, HackerNews, Reddit, etc.

**Tuesday**
- [x] Nudge merge requests forward
- [x] Manually look for a character count
- [x] Sync meeting and coffee chat
- [x] Look through navigation backlog again
- [x] Open issue for `[...]` tab on small viewports
- [x]] Skim the remote playbook

**Wednesday**
- [x] Triage the backlog with Christen & Taurie
- [x] Respond to a few todos
- [x] Look for issues to close

**Thursday**
- [x] Fix Shopify template merge request
- [x] Respond to compliance questions
- [x] Look into planning for 15.0
- [x] 10+ todos
- [x] Register for axe DevTools Pro

**Friday**
- [x] 🧦 Pajamas migration day 

♻️ Thoughts & Feelings


🗄 Backlog
- [ ] watch a few tutorials on [axe DevTools](https://axe.deque.com/)
- [ ] Take notes on user research sessions async x2
- [ ] Pick 3 items for seeking community contributions
- [ ] Finish Shopify stuff
- [ ] Settings needs some backlog love
- [ ] Check in on alerts improvements
- [ ] Refine Unboxing the Advanced Section epic
