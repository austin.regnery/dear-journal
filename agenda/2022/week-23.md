## WEEK 23 (2022), MILESTONE 15.1

### ⭐️ Goal: Get 🍔 menu ideas into testing

**Monday**

- [x] Merge request reviews
- [x] Epic additions
- [x] Breakdown testing plan into individual first click tests

**Tuesday**

- [x] Refine prototypes
- [x] Update test plan
- [x] Launch first test 🚀
- [x] Populate agendas

**Wednesday**

- [x] Sync meetings
- [x] Babysit control test in UserTesting

**Thursday**

- [x] UserTesting sessions
- [x] Design pair

**Friday**

- [x] Pajamas Migration Day 🧦


#### ♻️ Thoughts & Feelings

Busy week. Didn't plan on designing with Sid or helping the team with Pajamas Migration.

#### 🗄 Backlog

<details><summary>Ideas for another day</summary>

- [ ] Pick 3 items for seeking community contributions
- [ ] Settings needs some backlog love
- [ ] Check in on alerts improvements
- [ ] Refine Unboxing the Advanced Section epic

</details>
