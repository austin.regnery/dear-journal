## WEEK 19 (2022), MILESTONE 15.0

### ⭐️ Goal: Having a plan for Navigation in place for kickoff

> 8.7 hr in meetings (avg: 5.5 hr)

**Monday**
- [x] 🌴 Travel home from vacation

**Tuesday**
- [x] Catch back up on mentions
- [x] Implement translation feedack in merge request
- [x] Resolve failing pipelines
- [x] Take notes on user research session
- [x] UX Weekly
- [x] ☕️ chat
- [x] Start a milestone roadmap outline

**Wednesday**
- [x] Update gdk
- [x] Logged out navigation feedback
- [x] ☕️☕️ chat
- [x] Design pair
- [x] Navigation sync
- [x] Add namespaces to merge request

**Thursday**
- [x] Fix merge request
- [x] Open office hours
- [x] ☕️ chat
- [x] Touch base with Chritie
- [x] Continue refining navigation ideas
- [x] Notetaking for UX research

**Friday**
- [x] Merge request reviews and to-do catchup


#### ♻️ Thoughts & Feelings

I am wiped. I'm still excited about the direction for navigation but it is time to start executing and that's challenging to plan.

#### 🗄 Backlog
- [ ] Pick 3 items for seeking community contributions
- [ ] Settings needs some backlog love
- [ ] Check in on alerts improvements
- [ ] Refine Unboxing the Advanced Section epic
