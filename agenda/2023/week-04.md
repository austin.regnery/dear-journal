## WEEK 03 (2023), MILESTONE 15.9

**Monday**

- [x] Review research plan for favorites/pineed
- [x] Tag up with Nick
- [x] Start documenting gaps with examples

**Tuesday**
- [x] Coffee chats
- [x] Continue documenting hot button topics

**Wednesday**
- [x] Review performance feddback
- [x] Sync meetings
- [x] Continue documenting hot button topics

**Thursday**

Out sick 🤒

**Friday**

- [x] Catch up with Nick on Navigation proposals
- [x] Looks for more gaps