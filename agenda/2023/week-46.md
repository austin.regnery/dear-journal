- **Bias for action:** authored a change to a several year old suggestion → https://gitlab.com/gitlab-org/gitlab/-/merge_requests/135754
- **Directness:** saying no to a change in `gitlab-ui`, even when it's hard → https://gitlab.com/gitlab-org/gitlab-ui/-/merge_requests/3755#note_1641628308
- **Say why, not just what:** collaborated and informed wider community on changes to access the Admin Area → https://gitlab.com/gitlab-org/gitlab/-/issues/415854#note_1641852572
