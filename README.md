## What is this?

I created this personal README to help you get to know a little bit more about me, how I think, and how I work. I hope it can help us start having a conversation and build an awesome relationship together.

🦊 GitLab Work Handle - [`@aregnery`](https://gitlab.com/aregnery) <br>
💼 My Portfolio - https://austin-regnery.com/

## Hey there, I'm Austin (He/Him/His)

* Currently living in Charleston, South Carolina 🇺🇸 with my wife, Kelly, our dog [Sully](https://about.gitlab.com/company/team-pets/#260-sully), and son Parker.
* Raised in the suburbs of Atlanta, Georgia 🇺🇸 until I graduted in 2015 from [Georgia Tech](https://www.gatech.edu/).
* Each year my wife and I try to explore a new place to us for an extended period of time
  * 2016 - 🇮🇹 (Venice, Rome, Piza, Cinque Terre, Florence)
  * 2017 - 🇪🇸 (Madrid, Barcelona, Granda, Seville), 🇺🇬 (Masindi)
  * 2018 - 🇺🇸 (Hawaii – Maui, Honolulu, Oahu)
  * 2019 - 🇳🇿 (North & South Island)
  * 2020 - 🦠 (COVID-19 Lockdowns)
  * 2021 - 🇺🇸 (Hawaii – Maui)
  * 2022 - 👶🏼 We had our first kiddo
  * 2023 - 🇺🇸 (Denver and Nasville)
  * 2024 - 🇬🇷 (Sani Resort)


I am a Senior Product Designer working on the `Foundations` team. My first day with GitLab was June 8th, 2020 but here is a little more on me and my journey to getting here.

* I worked for almost 6 years at Boeing 🛫
* I haven't always been a designer, and I am proud of the fact that my educational background has only helped strengthen this passion. I wrote about [bootstrapping my design career on Medium](https://medium.com/@acregnery/bootstrapping-a-career-in-design-e882c71cb2b?source=friends_link&sk=c4bd6b885f5599a6924527192dfa84b4).
* I spent years helping grow the design community, designing for new and legcay applications, creating design systems from scratch, and mentoring new designers.
* I am too addicted to pixel perfect designs, so I am always trying to push myself to show early and often with low-fidelty. Call me out if I don't.
* My mind is usually thinking about what comes next, so if you got any forward thinking ideas then let's chat!
* As a product designer, I believe I am equally responsible for the product as product management and software engineering.
* My happy place leans towards the intersection of design and development. I love learning more about the front-end.
* I am still learning about how to do better User Reserach by expanding my methods and looking for how to leverage more quantitative data. 


### Personal principles

1. Be flexible with tools and processes.
1. Consistency is an expectation.
2. Impact the lives of others for the better.

## Feedback

Someone once told me that good feedback isn’t _cheap_. I usually get non-actionable and vague responses when I invest little energy in asking for specific feedback. I prefer to be asked for specific feedback (async), and recieve feedback in-person, so we can discuss. You will find me asking for ongoing feedback throughout the year on things I can do better, things I can improve, and things I am doing well.

## Scheduling

I keep my calendar as clear as possible. If you see a gap, then feel free to take it and I will tell you if I cannot make it happen _quickly_.

If you want or need to talk to me, and my schedule is not open, DM me on Slack and I will make sure we talk that day.

## Work hours

I have found working from 8:00 to 4:00 [EST](https://time.is/EST) with breaks throughout the day works best for me. I am more of a morning person, so early coffee chats are my fav. My wife is a Physician Assistant in the Emergency Department, and she works irregular hours and days so it's not uncommon to see me take some time periodically during the week to spend with her or my son.

## Communication style

My preferred method of communication is verbal. I am quite casual and love face-to-face interactions, and you will see that translated into my writting. I also like learning through narrative, so sometimes I will ask for a Zoom call if I need you to explain things to me.

I ask questions. I am not afraid of not knowing things, and I will seek for help whenever necessary. Although I prefer to search for things before I ask.

I also am opinionated. If I state something you disagree with, please feel free to challenge it – my goal is to incorporate feedback.

## Relationships

The people I work with are what truly matter to me in my day-to-day. I will be open and authentic with you, so keep that in mind if you ask me "how are you?". I also am happy to connect people to others, so please reach out if you want to chat!

I do my best to assume positive intent, and that you are here to deliver great value to our company and people. You are an expert in your craft, so I will work to provide necessary context and ask questions to help you vet your ideas but I will not try to override you.

Let me know if I am blocking you. Please reach out and let me know how I can help, especially if its design related.

## 1:1 meetings

I believe 1:1s are for you to set the agenda. What would you like to talk about? What is going well? What is bugging you? How can I help you? If there are things that I want to ask you, I will do it, but this is your time. If I have feedback, I will also provide it during this time.

By default, you should schedule a [30 minute conversation](https://calendly.com/aregnery/chat) with me as needed. I prefer that you schedule the meeting, just don't make it too late.

## Footnotes

This document is new and still a WIP and I would love your feedback! Did you find the time you spent reading this valuable? Was something critical missing? Feel free to go ahead and submit an MR to this document.

If you see me not living up to anything included please let me know 😉. It is possible that I have changed my mind, or that I am just dropping the ball. Either way, get in touch!

Inspired by [Rayan Verissimo's README](https://gitlab.com/rayana/readme) 
